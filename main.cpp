// imgui includes
#include "imgui.h"
#include "imgui-SFML.h"

// SFML includes
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/CircleShape.hpp>

////////////////////////////////////////////////////////////
// System includes
////////////////////////////////////////////////////////////
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <string>
#include <map>
#include <iostream>
#include <args.hxx>

// Local includes
#include "controller.hpp"

using HelpFlagType = args::HelpFlag;
using ControllerIndexArgType = args::ValueFlag<int>;
using DomainArgType = args::ValueFlag<int>;
using VerboseArgType = args::Flag;
using TypeLibraryArgType = args::ValueFlag<std::string>;
using DataWriterArgType = args::ValueFlag<std::string>;
using XmlFileNameArgType = args::ValueFlag<std::string>;
using DatatypeNameArgType = args::ValueFlag<std::string>;
using PublisherNameArgType = args::ValueFlag<std::string>;
using ParticipantNameArgType = args::ValueFlag<std::string>;
using AxisDivisorArgType = args::ValueFlag<float>;
using MinMaxRangeArgType = args::ValueFlag<float>;

int main(int argc, char** argv)
{
    /* create the parser */
    args::ArgumentParser parser("Control Console for remotely operated vehicles\n"
                                "Copyright 2018 Real-Time Innovations, Inc.");

    /* add flags to the parser */
    HelpFlagType           help(parser, "HELP", "Display this help menu", {'h', "help"});
    VerboseArgType         verbose(parser, "verbose", "output progess messages", {"v", "verbose"});
    ControllerIndexArgType ctrlr_idx(parser, "controller index", "select controller by index", { "i", "ctrlridx" }, 0);
    DomainArgType          domain(parser, "domain", "domain to join", {"d", "domain"}, 0);
    XmlFileNameArgType     xmlfile(parser, "xml", "Specify XML file", {"x", "xml"}, "/Users/rennie/demos/donkey/pc/vehicle/Controller.xml");
    TypeLibraryArgType     type_library(parser, "type library", "library that contains types", {"t", "typelib"}, "ControllerTypes");
    DataWriterArgType      dwname(parser, "datawriter", "Specify data writer name (for controller)", {"w", "writer"}, "CommandDataWriter");
    DatatypeNameArgType    ctrlr_dt_name(parser, "controller", "Specify controller type name", {"c", "ctrlrtype"}, "ControlType");
    PublisherNameArgType   pubname(parser, "publisher", "Specify publisher name", {"p", "publisher"}, "ControllerPublisher");
    ParticipantNameArgType partname(parser, "participant", "Specify participant name", {"P", "participant"}, "ControllerParticipantLibrary::ControllerParticipant");
    AxisDivisorArgType     axis_div(parser, "axis divisor", "Divisor for axis value to scale to consumers range", {"a", "axisdiv"}, 100.0 );
    MinMaxRangeArgType     min_range_val(parser, "min acceptable", "Minimum acceptable value for an axis", {"m", "minaxis"}, -1.0);
    MinMaxRangeArgType     max_range_val(parser, "max acceptable", "Maximum acceptable value for an axis", {"M", "maxaxis"}, 1.0);
    
    /* complete parsing definition */
    args::CompletionFlag completion(parser, {"complete"});

    try {
        /* parse what is available in argv */
        parser.ParseCLI(argc, argv);
    } catch (args::Completion e) {
        std::cout << e.what();
        return -1;
    } catch (args::Help) {
        std::cout << parser;
        return 0;
    } catch (args::ParseError e) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return -2;
    }

    std::string type_name(type_library.Get() + "::" + ctrlr_dt_name.Get());
    std::string dw_name(pubname.Get() + "::" + dwname.Get());
    TypeWriter types = { { type_name, dw_name } };

    Controller ctrlr(domain.Get(), xmlfile.Get(), type_library.Get(), types, partname.Get(),
        [&ctrlr, &dw_name, &ctrlr_idx, &axis_div, &min_range_val, &max_range_val]() -> void
        {
            // reserve a sample to populate
            int sample = ctrlr.reserve_sample(dw_name);

            // read the incoming data necessary to populate controller
            double steering = clamp(sf::Joystick::getAxisPosition(ctrlr_idx.Get(), 
                                                                  static_cast<sf::Joystick::Axis>(Steering)
                                                                 ) / axis_div.Get(),
                                    min_range_val.Get(), max_range_val.Get()
                                   );
            double accel = clamp(sf::Joystick::getAxisPosition(ctrlr_idx.Get(), 
                                                               static_cast<sf::Joystick::Axis>(Acceleration)
                                                              ) / axis_div.Get(),
                                 min_range_val.Get(), max_range_val.Get()
                                );                          
            bool   enable = sf::Joystick::isButtonPressed(ctrlr_idx.Get(), Enable);

            // bind the incoming data to the outgoing data
            ctrlr.set_sample_value(sample, "acceleration", -accel);
            ctrlr.set_sample_value(sample, "steering", steering);
            ctrlr.set_sample_value(sample, "enable", enable);

            // send data out
            ctrlr.write_sample(dw_name, sample);

            ctrlr.return_sample(sample);
        });

    sf::RenderWindow window(sf::VideoMode(1280, 720), "ImGui + SFML = <3");
    window.setFramerateLimit(60);
    ImGui::SFML::Init(window);

    sf::CircleShape shape(100.f);
    shape.setFillColor(sf::Color::Green);

    sf::Clock deltaClock;
    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            ImGui::SFML::ProcessEvent(event);

            ctrlr.update();

            if (event.type == sf::Event::Closed) {
                window.close();
            }
        }

        ImGui::SFML::Update(window, deltaClock.restart());

        ImGui::Begin("Hello, world!");
        ImGui::Button("Look at this pretty button");
        ImGui::End();

        window.clear();
        window.draw(shape);
        ImGui::SFML::Render(window);
        window.display();
    }

    ImGui::SFML::Shutdown();
}
