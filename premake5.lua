-- premake5.lua
workspace "GUI"
   configurations { "Debug", "Release" }

project "Donkey"
    language "C++"
    targetdir "bin/%{cfg.buildcfg}"

    includedirs { "imgui", "imgui-sfml" }

    links { 
        "sfml-audio",
        "sfml-graphics",
        "sfml-network",
        "sfml-system",
        "sfml-window"
    }

    configurations { "Debug", "Release" }
        filter  { "system:macosx" }
            kind "ConsoleApp"
            toolset "clang"
            buildoptions { "-std=c++17", "-m64" }
            defines { "RTI_UNIX -DRTI_DARWIN", "RTI_DARWIN16", "RTI_64BIT", "RTIBOOST_NO_AUTO_PTR" }
            includedirs { "/usr/local/include/SFML",
                          "/usr/local/opt/include", 
                          "/Applications/rti_connext_dds-5.3.1/include", 
                          "/Applications/rti_connext_dds-5.3.1/include/ndds",
                          "/Applications/rti_connext_dds-5.3.1/include/ndds/hpp"
            }
            libdirs { "/usr/local/lib",
                      "/Applications/rti_connext_dds-5.3.1/lib/x64Darwin17clang9.0"
            }
            linkoptions { "-framework OpenGL" }
            links { "z", "m", "nddscpp2", "nddsc", "nddscore" }
       
    files { "*.h", "*.hpp", "*.cpp", "imgui/imgui.cpp", "imgui/imgui_draw.cpp", "imgui-sfml/imgui-SFML.cpp" }

    filter "configurations:Debug"
        defines { "DEBUG" }
        symbols "On"

    filter "configurations:Release"
        defines { "NDEBUG" }
        optimize "On" 
