// DDS includes
#include <dds/dds.hpp>
#include <rti/config/Version.hpp>
#include <rti/core/QosProviderParams.hpp>
#include <rti/topic/PrintFormat.hpp>
#include <rti/topic/rtitopic.hpp>

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <string>
#include <map>
#include <iostream>

#include "controller.hpp"

namespace rcore = rti::core;
namespace dcore = dds::core;
using namespace dds::core::status;
using namespace dds::domain;
using namespace dds::topic;
using namespace dds::pub;
using namespace dds::sub;

template <typename T>
struct Pub {
    Pub(DomainParticipant& participant,
        const dcore::QosProvider& type_accessor,
        const std::string& type_lib,
        const std::string& type_name,
        const std::string& dw_name):
        m_participant(participant),
        m_data_type(static_cast<const dcore::xtypes::StructType&>(type_accessor->type(type_lib, type_name))),
        m_dw(rti::pub::find_datawriter_by_name<DataWriter<T>>(participant, dw_name))
    {
    }
    ~Pub()
    {
    }

    void write(T sample)
    {
        m_dw.write(sample);
    }

    DomainParticipant&      m_participant;
    T                       m_data_type;
    DataWriter<T>           m_dw;
};

template <typename T>
struct Sub {
    Sub(DomainParticipant& participant,
        const dcore::QosProvider& type_accessor,
        const std::string& type_lib,
        const std::string& type_name,
        const std::string& dr_name):
        m_participant(participant),
        m_data_type(static_cast<const dcore::xtypes::StructType&>(type_accessor->type(type_lib, type_name))),
        m_dr(rti::sub::find_datareader_by_name<DataWriter<T>>(participant, dr_name))
    {
    }
    ~Sub()
    {
    }

    T& take(void)
    {
        return m_dr.take();
    }

    T& read(void)
    {
        return m_dr.read();
    }

    DomainParticipant&      m_participant;
    T                       m_data_type;
    DataReader<T>           m_dr;
};

static std::unique_ptr<rcore::QosProviderParams> get_provider_params(const std::string& xml_file_name) 
{
    std::unique_ptr<rcore::QosProviderParams> provider_params(std::make_unique<rcore::QosProviderParams>(rcore::QosProviderParams()));

    provider_params->url_profile(dcore::StringSeq(1, xml_file_name));
    dcore::QosProvider::Default()->default_provider_params(*provider_params);

    return provider_params;
}

static const TypeWriter& register_types(const TypeWriter& types)
{
    // must register all types before we can create the domain participant
    for(auto type_name: types) {
        rti::domain::register_type<dcore::xtypes::DynamicData>(type_name.first);
    }

    return types;
}

struct ControllerImpl {
    using GenericDw = Pub<dcore::xtypes::DynamicData>;
    using MapEntry = std::pair<std::string, std::unique_ptr<GenericDw>>;

    ControllerImpl(int domain_id,
                   const std::string& xml_file_name,
                   const std::string& type_lib,
                   const TypeWriter types,
                   const std::string& participant_name,
                   std::function<void ()> func):
        m_xml_file_name(xml_file_name),
        m_type_lib(type_lib),
        m_provider_params(get_provider_params(xml_file_name)),
        m_types(register_types(types)),
        m_type_accessor(xml_file_name),
        m_params(domain_id),
        m_participant(dcore::QosProvider::Default()->create_participant_from_config(participant_name, 
                                                                                    m_params)
                     ),
        m_func(func)
    {
        for(auto type_name: m_types) {
            m_writers.insert(std::pair<std::string, 
                             std::unique_ptr<GenericDw>>(type_name.second,
                                                         std::make_unique<GenericDw>(GenericDw(m_participant,
                                                                                               m_type_accessor,
                                                                                               m_type_lib,
                                                                                               type_name.first,
                                                                                               type_name.second)
                                                                                    )
                                                        )
                            );
        }
    }

    ~ControllerImpl()
    {
    }

    void update(void) 
    {
        m_func();
    }

    int reserve_sample(const std::string& dw_name)
    {
        m_samples.insert(std::pair<int, dcore::xtypes::DynamicData&>(0, m_writers[dw_name]->m_data_type));
        return 0;
    }

    void set_sample_value(int sample_idx, const std::string& member_name, double value)
    {
        auto i = m_samples.find(sample_idx);

        if(i != m_samples.end()) {
            i->second.value<double>(member_name, value);
        }
    }

    void set_sample_value(int sample_idx, const std::string& member_name, bool value)
    {
        auto i = m_samples.find(sample_idx);

        if(i != m_samples.end()) {
            i->second.value<bool>(member_name, value);
        }
    }
 
    void write_sample(const std::string& dw_name, int sample_idx)
    {
        auto i = m_samples.find(sample_idx);

        if(i != m_samples.end()) {
            m_writers[dw_name]->write(i->second);
        }
    }

    std::string                                                  m_xml_file_name;
    std::string                                                  m_type_lib;
    std::unique_ptr<rcore::QosProviderParams>                    m_provider_params;
    const TypeWriter&                                            m_types;
    dcore::QosProvider                                           m_type_accessor;
    rti::domain::DomainParticipantConfigParams                   m_params;
    DomainParticipant                                            m_participant;
    std::map<std::string, std::unique_ptr<GenericDw>>            m_writers;
    std::function<void ()>                                       m_func;   

    std::map<int, dcore::xtypes::DynamicData&>                   m_samples;
};

Controller::Controller(int domain_id,
                       const std::string& xml_file_name,
                       const std::string& type_lib,
                       const TypeWriter types,
                       const std::string& participant_name,
                       std::function<void ()> func)
{
    m_impl = std::make_unique<ControllerImpl>(domain_id, 
                                              xml_file_name, 
                                              type_lib, 
                                              types, 
                                              participant_name,
                                              func
                                             );
}

Controller::~Controller()
{
}

void Controller::update(void)
{
    m_impl->update();
}

int Controller::reserve_sample(const std::string& dw_name)
{
    return m_impl->reserve_sample(dw_name);
}

void Controller::set_sample_value(int sample_idx, const std::string& member_name, double value)
{
    return m_impl->set_sample_value(sample_idx, member_name, value);
}

void Controller::set_sample_value(int sample_idx, const std::string& member_name, bool value)
{
    return m_impl->set_sample_value(sample_idx, member_name, value);
}

void Controller::write_sample(const std::string& dw_name, int sample_idx)
{
    m_impl->write_sample(dw_name, sample_idx);
}

void Controller::return_sample(int sample_idx)
{
    // NULL OP
}

double clamp(float value, float min, float max)
{
    return std::max(std::min(value, max), min);
}
