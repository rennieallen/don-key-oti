#include <algorithm>
#include <sstream>
#include <iomanip>
#include <string>
#include <map>
#include <iostream>
#include <functional>

using TypeReader = std::vector<std::pair<std::string, std::string>>;
using TypeWriter = std::vector<std::pair<std::string, std::string>>;

struct ControllerImpl;

using ControllerAxis = enum 
{
    Steering = 0,
    Acceleration = 1,
    Cruise = 2
};
using ControllerButtons = enum 
{
    Enable = 0
};

double clamp(float val, float min, float max);

/**
 * @brief Controller. 
 * 
 * Moves data from a controller like object (e.g. a joystick) into the the context
 * of a Commands topic in DDS in a generic manner. The genericity is derived from the 
 * fact the the client code supplies a lambda that transforms the context from the 
 * controller domain to the DDS domain.  Sample reservation, manipulation and sample
 * delivery request methods are exposed in the public interface.  Typically the client
 * codes lambda (supplied as the last parameter) captures the clients instance of the 
 * Controller, along with anything else from the context it may require in order to be
 * able to facilitate the transformation from the controller space, into the DDS space.
 * 
 * All DDS management is automatically handled transparently.
 * 
 */
struct Controller {
    Controller(int domain_id,
               const std::string& xml_file_name,
               const std::string& type_lib,
               const TypeWriter types,
               const std::string& participant_name,
               std::function<void ()> func);

    ~Controller();

    void update(void);

    int  reserve_sample(const std::string& dw_name);
    void set_sample_value(int sample_id, const std::string& member_name, double value);
    void set_sample_value(int sample_id, const std::string& member_name, bool value);
    void write_sample(const std::string& dw_name, int sample_id);
    void return_sample(int sample_id);

    std::unique_ptr<ControllerImpl>  m_impl;
};
